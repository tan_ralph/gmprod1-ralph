﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorSpecial01 : MonoBehaviour {

    public bool isLock;
    public bool isOpen;
    public GameObject bridgePlatform;

	// Use this for initialization
	void Start () {
        isLock = false;
        isOpen = false;

    }
	
	// Update is called once per frame
	void Update () {

        //boss door
        if (transform.childCount != 0)
        {
            if (transform.GetChild(0).GetComponent<BossDoor>().isLock)
                transform.GetChild(1).GetComponent<BossDoor>().isLock = true;

            if (transform.GetChild(1).GetComponent<BossDoor>().isLock)
                transform.GetChild(0).GetComponent<BossDoor>().isLock = true;
        }

        //bridge
        if (isOpen)
        {
            bridgePlatform.transform.localPosition = new Vector3(bridgePlatform.transform.localPosition.x, 50.0f, bridgePlatform.transform.localPosition.z);
        }

	}

    public void  OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOpen = true;
        }
    }
}
