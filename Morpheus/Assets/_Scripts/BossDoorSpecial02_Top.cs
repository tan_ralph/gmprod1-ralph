﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorSpecial02_Top : MonoBehaviour {

    public bool goBot;
    public GameObject triggerBot;

    public void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("GoBot");
        if (collision.gameObject.tag == "Elevator")
        {
            goBot = true;
            triggerBot.GetComponent<BossDoorSpecial02_Bot>().goTop = false;
        }
    }

    // Use this for initialization
    void Start () {
        goBot = true;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

}
