﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : FSMState {

    public EnemyAttack()
    {
        stateID = StateID.Attack;
    }

    public override void Reason(GameObject player, GameObject npc)
    {

    }

    public override void Act(GameObject player, GameObject npc)
    {

    }

}
