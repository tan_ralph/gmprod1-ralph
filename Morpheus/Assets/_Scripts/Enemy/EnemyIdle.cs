﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdle : FSMState {

    public EnemyIdle()
    {
        stateID = StateID.Idle;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (npc.GetComponent<DetectPlayer>().playerDetected)
            npc.GetComponent<EnemyFSM>().SetTransition(Transition.DetectPlayer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        
    }

}
