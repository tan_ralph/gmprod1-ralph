﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : FSMState {

    public EnemyPatrol()
    {
        stateID = StateID.Patrol;
    }

    public override void Reason(GameObject player, GameObject npc)
    {

    }

    public override void Act(GameObject player, GameObject npc)
    {
        
    }

}
