﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Pickup : Interactable {
    
    public ComponentSO component;

    public Pickup()
    {
        type = InteractableType.pickup;
    }

    public override void Interact()
    {
        base.Interact();
        if (!component.isAutoPickup)
        {
            if (Input.GetKeyDown(KeyCode.E))
                PickUp();
        }
        else
            PickUp();
    }

    void PickUp()
    {
        if (!Inventory.instance.IsFull())
            Inventory.instance.Add(component);
        else
            Inventory.instance.PickUpOnFull(component);        

        Destroy(gameObject);
    }

}