﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Player))]
public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    public Image[] slots;

    public List<ComponentSO> equipped = new List<ComponentSO>();
    public List<ComponentBase> equipped2 = new List<ComponentBase>();

    [Header("Events")]
    [Space]
    public UnityEvent OnInventoryChange;


    PlayerData player;

    public void Start()
    {
        player = GetComponent<Player>().data;

        if (OnInventoryChange == null)
            OnInventoryChange = new UnityEvent();

        SetMaxSlots(player.currentMaxSlots);

    }

    void Update()
    {

    }

    public void SetMaxSlots(int max)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < max; i++)
        {
            slots[i].gameObject.SetActive(true);
            slots[i].transform.Find("Sprite").GetComponent<Image>().enabled = false;
        }
    }

    public void UpdateUISlots()
    {
        int max = player.currentMaxSlots;

        if (equipped.Count == 0)
        {
           foreach(Image slot in slots)
            { 
                if (slot.gameObject.activeSelf)
                    slot.transform.Find("Sprite").GetComponent<Image>().enabled = true;
            }
            return;
        }

        for (int i = 0; i < max; i++)
        {
            if (slots[i].gameObject.activeSelf)
            {
                var image = slots[i].transform.Find("Sprite").GetComponent<Image>();
                image.sprite = equipped[i].icon;
                image.enabled = true;
            }
        }
    }

    //void EnableUISlot(int index)
    //{
    //    slots[index].enabled = true;
    //}

    //void DisableUISlot(int index)
    //{
    //    slots[index].enabled = false;
    //}

    //public void UpdateUISlot(int slotIndex, int equipIndex, bool active)
    //{
    //    if(!active)
    //    {
    //        DisableUISlot(slotIndex);
    //        return;
    //    }

    //    EnableUISlot(slotIndex);
    //    slots[slotIndex].sprite = equipped[equipIndex].icon;
    //}

    public void Add(ComponentSO component)
    {
        equipped.Add(component);
        OnInventoryChange.Invoke();
    }

    public void Remove(ComponentSO component)
    {
        equipped.Remove(component);
        OnInventoryChange.Invoke();
    }

    public void PickUpOnFull(ComponentSO component)
    {
        Remove(equipped[0]);
        Add(component);
    }

    public bool IsFull()
    {
        if (equipped.Count == player.currentMaxSlots)
            return true;
        else
        {

        }
            return false;
    }

    public void Clear()
    {
        equipped.Clear();
        OnInventoryChange.Invoke();
    }

    public int GetCount(ComponentBase component)
    {
        int count = 0;
        foreach (ComponentBase comp in equipped2)
        {
            if (comp.name == component.name)
                count++;
        }
            return count;
    }

}