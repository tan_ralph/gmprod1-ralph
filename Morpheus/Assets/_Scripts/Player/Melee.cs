﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour {

    public LayerMask enemyLayer;

    PlayerController control;
    PlayerData data;
    bool doDamage = false;

    void Start()
    {
        data = gameObject.GetComponentInParent<Player>().data;
        control = gameObject.GetComponentInParent<PlayerController>();

        if (data == null)
            Debug.LogError(transform.name + ": (Melee) is missing Player component in parent");

        if (data == null)
            Debug.LogError(transform.name + ": (Melee) is missing PlayerController component in parent");
    }

    void Update()
    {
        
    }

    public bool DoDamage { get { return doDamage; } set { doDamage = value; } }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collision detected");
        if (control.meleeTrigger && collision.tag == "Enemy")
        {
            Debug.Log("Enemy hit!");
            collision.GetComponent<Enemy>().TakeDamage(data.Attack);
            control.meleeTrigger = false;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            collision.GetComponent<Enemy>().TakeDamage(data.currentAttack);
        }
    }
}
