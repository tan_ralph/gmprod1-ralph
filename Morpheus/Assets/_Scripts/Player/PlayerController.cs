﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public enum AnimIndex
{
    Idle = 0,
    Walk = 1,
    Run = 2,
    Jump = 3,
    Damage = 4,
    Die = 5
}

[RequireComponent(typeof(Controller2D))]
public class PlayerController : MonoBehaviour
{
    public PlayerControllerData data;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;
    public bool isFacingRight;
    public bool meleeTrigger;

    public float radius;
    public LayerMask enemyLayer;
    public Collider2D meleeHitbox;
    public Transform firePoint;

    float timeToWallUnstick;

    float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    Controller2D controller;

    Vector2 directionalInput;
    bool wallSliding;
    int wallDirX;

    Animator anim;
    SpriteRenderer sprite;

    [Header("Events")]
    [Space]
    public UnityEvent OnMeleeEvent;


    void Start()
    {
        controller = GetComponent<Controller2D>();

        anim = GetComponentInChildren<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    
        gravity = -(2 * data.MaxJumpHeight) / Mathf.Pow(data.TimeUntilJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * data.TimeUntilJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * data.MinJumpHeight);
    }

    void Update()
    {        
        CalculateVelocity();
        HandleWallSliding();
        
        controller.Move(velocity * Time.deltaTime, directionalInput);
        meleeHitbox.GetComponent<HorizontalFlipper>().isFacingRight = isFacingRight;

        UpdateAnimation();
        
        if (controller.collisions.above || controller.collisions.below)
        {
            
            if (controller.collisions.slidingDownMaxSlope)
            {
                velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
                OnLand();
            }
        }
    }

    #region Animation
    void UpdateAnimation()
    {
        if (directionalInput.x < 0 && !wallSliding)
        {
            isFacingRight = true;
            sprite.flipX= false;
        }
        if(directionalInput.x > 0 && !wallSliding)
        {
            isFacingRight = false;
            sprite.flipX = true;
        }

        if (directionalInput.x != 0)
        {
            anim.SetInteger("Index", (int)AnimIndex.Run);
        }
        if (directionalInput.x == 0)
        {
            anim.SetInteger("Index", (int)AnimIndex.Idle);
        }

        if (directionalInput.y > 0)
        {
            anim.SetTrigger("Jump");
        }

        anim.SetBool("WallSlide", wallSliding);
        
        if(meleeTrigger)
        {
            anim.SetTrigger("Melee");
            meleeTrigger = false;
        }
    }

    public void OnMelee()
    {
        meleeTrigger = true;
        gameObject.GetComponent<Player>().ApplyDamage(meleeHitbox, enemyLayer);
    }

    public void OnRangedAttack()
    {

    }

    public void OnLand()
    {
        anim.SetBool("Jump", false);
    }

    public void OnWallSlide()
    {
        //anim.SetBool("WallSlide", true);
    }

    public void OnTakeDamage()
    {
        anim.SetTrigger("Damage");
    }
    #endregion

    #region Controls (Move, Jump, Wall Slide, Velocity)

    public void SetDirectionalInput(Vector2 input)
    {
        directionalInput = input;
    }

    public void OnJumpInputDown()
    {
        if (wallSliding)
        {
            if (wallDirX == directionalInput.x)
            {
                velocity.x = -wallDirX * wallJumpClimb.x;
                velocity.y = wallJumpClimb.y;
            }
            else if (directionalInput.x == 0)
            {
                velocity.x = -wallDirX * wallJumpOff.x;
                velocity.y = wallJumpOff.y;
            }
            else
            {
                velocity.x = -wallDirX * wallLeap.x;
                velocity.y = wallLeap.y;
            }
        }
        if (controller.collisions.below)
        {
            if (controller.collisions.slidingDownMaxSlope)
            {
                if (directionalInput.x != -Mathf.Sign(controller.collisions.slopeNormal.x))
                { // not jumping against max slope
                    velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
                    velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
                }
            }
            else
            {
                velocity.y = maxJumpVelocity;
            }
        }
    }

    public void OnJumpInputUp()
    {
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }


    void HandleWallSliding()
    {
        wallDirX = (controller.collisions.left) ? -1 : 1;
        wallSliding = false;
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;
            if (velocity.y < -data.WallSlideSpeedMax)
            {
                velocity.y = -data.WallSlideSpeedMax;
            }

            if (timeToWallUnstick > 0)
            {
                velocityXSmoothing = 0;
                velocity.x = 0;

                if (directionalInput.x != wallDirX && directionalInput.x != 0)
                {
                    timeToWallUnstick -= Time.deltaTime;
                }
                else
                {
                    timeToWallUnstick = data.WallStickTime;
                }
            }
            else
            {
                timeToWallUnstick = data.WallStickTime;
            }
        }
    }

    void CalculateVelocity()
    {
        float targetVelocityX = directionalInput.x * data.MoveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? data.AccelerationTimeGrounded : data.AccelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
    }
    #endregion

    #region Gizmos
    void OnDrawGizmosSelected()
    {

    }
    #endregion

}
