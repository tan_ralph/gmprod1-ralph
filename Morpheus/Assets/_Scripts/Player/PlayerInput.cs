﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerInput : MonoBehaviour
{
    public float attackCooldown = 2.0f;
    float timeUntilNextAttack = 0.0f;
    
    Vector2 directionalInput;
    PlayerController controller;

    void Start()
    {
        controller = GetComponent<PlayerController>();
    }

    void Update()
    {
        // Movement
        directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        controller.SetDirectionalInput(directionalInput);

        // Jumping
        if (Input.GetKeyDown(controller.data.jump))
        {
            controller.OnJumpInputDown();
        }
        if (Input.GetKeyUp(controller.data.jump))
        {
            controller.OnJumpInputUp();
        }

        // Attacking
        if (timeUntilNextAttack <= 0 && Input.GetKeyDown(controller.data.attack))
        {
            if(controller.data.isMelee)
                controller.OnMelee();
            if (controller.data.isRanged)
                controller.OnRangedAttack();

            timeUntilNextAttack = attackCooldown;
        }
        else
            timeUntilNextAttack -= Time.deltaTime;

        // Clearing inventory slots
        if(Input.GetKeyDown(controller.data.purge))
        {
            Inventory.instance.Clear();
        }


        // Pausing
        if(Input.GetKeyDown(controller.data.pause))
        {
            Debug.Log("pause");
        }
    }

}
