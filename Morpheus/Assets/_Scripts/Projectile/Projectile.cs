﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Projectile : MonoBehaviour {
    
    public float speed;
    public Vector2 direction;
    //public Transform firePoint;

    [SerializeField]
    float range;
    [SerializeField]
    protected int damage;

    Vector2 origin;
    Rigidbody2D rb;

	void Start ()
    {
        origin = transform.position;
        rb = GetComponent<Rigidbody2D>();
        //if (firePoint == null)
        //    Debug.Log("Fire point not set");

    }
	
	void FixedUpdate ()
    {
        rb.MovePosition(rb.position + direction * speed * Time.deltaTime);
        float distanceFromOrigin = Vector2.Distance(transform.position, origin);

        if (distanceFromOrigin >= range)
            Destroy(gameObject);
    }
    public int Damage { get { return damage; } set { damage = value; } }

    public void SetTargetDirection(Vector2 dir)
    {
        direction = dir;
        if (direction.magnitude != 1)
            direction = direction.normalized;
    }
}
