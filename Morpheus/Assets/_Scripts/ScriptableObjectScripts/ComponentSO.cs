﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ComponentType
{
    Frame,
    Energy
}

[CreateAssetMenu(fileName = "NewComponent", menuName = "Component")]
public class ComponentSO : ScriptableObject {
    
    public ComponentType componentType;
    public string componentName;
    public Sprite icon = null;
    public bool isEquipped = false;
    public bool isAutoPickup = true;

    public int attackBonus;

    public virtual void ApplyEffect()
    {

    }

    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }

}
