﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewControllerData", menuName = "Player Controller Data")]
public class PlayerControllerData : ScriptableObject {

    //Defaults
    [Header("Movement")]
    float maxJumpHeight = 4;
    float minJumpHeight = 1;
    float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    float moveSpeed = 6;
    float wallSlideSpeedMax = 3;
    float wallStickTime = .25f;

    public float mMaxJumpHeight;
    public float mMinJumpHeight;
    public float mTimeUntilJumpApex;
    public float mAccelerationTimeAirborne;
    public float mAaccelerationTimeGrounded;
    public float mMoveSpeed;
    public float mWallSlideSpeedMax;
    public float mWallStickTime;

    public bool isJumping = false;

    [Header("Controls")]
    [Space]
    public KeyCode left;
    public KeyCode right;
    public KeyCode jump;
    public KeyCode attack;
    public KeyCode interact;
    public KeyCode purge;
    public KeyCode pause;

    [Header("Combat")]
    public float meleeCooldown;
    public bool isMelee = false;
    public float fireRate;
    public bool isRanged = false;



    private void Awake()
    {
        mMaxJumpHeight = maxJumpHeight;
        mMinJumpHeight = minJumpHeight;
        mTimeUntilJumpApex = timeToJumpApex;
        mAccelerationTimeAirborne = accelerationTimeAirborne;
        mAaccelerationTimeGrounded = accelerationTimeGrounded;
        mMoveSpeed = moveSpeed;
        mWallSlideSpeedMax = wallSlideSpeedMax;
        mWallStickTime = wallStickTime;
    }

    public float MaxJumpHeight { get { return mMaxJumpHeight; } set { mMaxJumpHeight = value; } }
    public float MinJumpHeight { get { return mMinJumpHeight; } set { mMinJumpHeight = value; } }
    public float TimeUntilJumpApex { get { return mTimeUntilJumpApex; } set { mTimeUntilJumpApex = value; } }
    public float AccelerationTimeAirborne { get { return mAccelerationTimeAirborne; } set { mAccelerationTimeAirborne = value; } }
    public float AccelerationTimeGrounded { get { return mAaccelerationTimeGrounded; } set { mAaccelerationTimeGrounded = value; } }
    public float MoveSpeed { get { return mMoveSpeed; } set { mMoveSpeed = value; } }
    public float WallSlideSpeedMax { get { return mWallSlideSpeedMax; } set { mWallSlideSpeedMax = value; } }
    public float WallStickTime { get { return mWallStickTime; } set { mWallStickTime = value; } }

}
