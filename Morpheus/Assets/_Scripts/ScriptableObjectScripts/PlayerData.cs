﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPlayerData", menuName = "Player Data")]
public class PlayerData : ScriptableObject
{
    //int maxHealth = 1000;
    //int init_attack = 100;
    //int maxSlots = 3;

    //public ComponentSO[] slots;

    //public int m_CurrentHealth;
    //public int m_Attack;
    //public int m_CurrentMaxSlots;

    //private void Start()
    //{
    //    m_CurrentHealth = maxHealth;
    //    m_Attack = init_attack;
    //    m_CurrentMaxSlots = 1;
    //}

    //public int MaxHealth { get { return maxHealth; } }
    //public int MaxSlots { get { return maxSlots; } }

    //public int Health { get { return m_CurrentHealth; } set { m_CurrentHealth = value; } }
    public int Attack { get{ return currentAttack; } set { currentAttack = value; } }
    //public int CurrentMaxSlots { get { return m_CurrentMaxSlots; } set { m_CurrentMaxSlots = value; } }

    [SerializeField]
    int init_maxHealth = 1000;
    [SerializeField]
    int init_attack = 100;
    [SerializeField]
    int init_maxSlots = 1;

    public int currentMaxHealth;
    public int currentHealth;
    public int currentAttack;
    public int currentMaxSlots;

    public int InitMaxHealth { get { return init_maxHealth; } }
    public int InitAttack { get { return init_attack; } }
    public int InitMaxSlots { get { return init_maxSlots; } }

}
