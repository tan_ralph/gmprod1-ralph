﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ComponentBase : ScriptableObject {

    public string componentName;
    public Sprite sprite;

    public virtual void ApplyEffect()
    {

    }

    public bool HasDuplicate()
    {
        if (Inventory.instance.GetCount(this) > 1)
            return true;
        else
            return false;
    }

}
